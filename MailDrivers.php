<?php
/**
 * Абстрактный клас для отправки писем
 */
abstract class MailDrivers extends CComponent
{
	/**
	 * Объект класа Mail
	 *
	 * @var Mail
	 */
	protected $_classMail;
	
	/**
	 * Кодировка письма
	 *
	 * @var string
	 */
	protected $_charset = 'utf-8';
	
	/**
	 * Тип письма
	 *
	 * @var string
	 */
	protected $_type = 'text/html';
	
	/**
	 * Фабрика создания драйвера для отправки почты
	 *
	 * @param string $name
	 * @return MailDrivers
	 */
	public static function factory($name)
	{
		Yii::import('ext.msSendMail.drivers.'.$name);

		return new $name;
	}
	
	/**
	 * Отправка письма
	 *
	 * @throws CException
	 * @param Mail $classMail
	 * @return mixed
	 */
	public function send(Mail $classMail = null)
	{
		if (!is_null($classMail))
		{
			$this->setClassMail($classMail);
		}

		if (!isset($this->_classMail))
		{
			throw new CException('Не задан ClassMail');
		}

		//Вызов механизма отправки писем 
		return $this->_send();
	}

	/**
	 * Задать обект Mail
	 *
	 * @param Mail $classMail
	 * @return MailDrivers
	 */
	public function setClassMail(Mail $classMail)
	{
		$this->_classMail = $classMail;

		return $this;
	}

	/**
	 * Создание заголовка письма
	 *
	 * @return array
	 */
	protected function _createHeader()
	{
		$header = array();

		//Преобразовать массив соответствия параметров шапки письма с переменными класса
		foreach ($this->_classMail->getMap() AS $name => $attr)
		{
			$v = call_user_func(array($this->_classMail, $attr));
			if (isset($v))
			{
				$header[] = $name.": ".$v;
			}
		}
		
		return $header;
	}

	/**
	 * Получает заголовок письма
	 *
	 * @throws CException
	 * @return array
	 */
	public function getHeader()
	{
		if (!isset($this->_classMail))
		{
			throw new CException('Не задан ClassMail');
		}

		return $this->_createHeader();
	}

	/**
	 * Механизм отправки почты
	 *
	 * @return bool
	 */
	abstract protected function _send();
}