<?php

Yii::import('ext.msSendMail.MailDrivers');

/**
 * Класс для отправки почты через sendmail
 *
 */

class DSendMail extends MailDrivers
{
	/**
	 * Создание заголовка письма
	 *
	 * @return array
	 */
	protected function _createHeader()
	{
		$header = array();

		$header[] = "MIME-Version: 1.0";
		$header[] = "Content-Type: ".$this->_type."; charset=".$this->_charset;

		return array_merge(parent::_createHeader(), $header);
	}

	/**
	 * Механизм отправки почты
	 *
	 * @return bool
	 */
	protected function _send()
	{
		//Создание заголовка
		$header = implode("\r\n", $this->_createHeader());

		//отправка письма
		return @mail($this->_classMail->getTo(),
			$this->_classMail->getSubject(),
			$this->_classMail->getMessage(),
			$header);
	}
}