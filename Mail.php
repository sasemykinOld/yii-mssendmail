<?php
/**
 * Отправка почты
 *
 * @method array  getMap()
 * @method string getTo()
 * @method string getFrom()
 * @method string getMessage()
 * @method string getSubject()
 */
class Mail extends CComponent
{
	/**
	 * E-Mail отправителя
	 *
	 * @var string
	 */
	protected $_from;
	
	/**
	 * Получатель(и) письма
	 *
	 * @var string
	 */
	protected $_to;
	
	/**
	 * Текст письма
	 *
	 * @var string
	 */
	protected $_message;
	
	/**
	 * Заголовок письма
	 *
	 * @var string
	 */
	protected $_subject;
	
	/**
	 * Название драйвера отправки писем
	 *
	 * @var string
	 */
	protected $_drivers = 'DSendMail';
	
	/**
	 * Соответствие параметров шапки письма с переменными класса
	 *
	 * @var array
	 */
	protected $_map = array(
			'From'  => 'getFrom',
			//'Cc'    => '_cc',
			//'Bcc'   => '_bcc',
		);
	
	/**
	 * Метод для "отлова" всех нереализованных в данном классе методов
	 *
	 * @param  string $name
	 * @param  array  $parameters
	 * @return mixed
	 */
	public function __call($name, $parameters)
	{
		//Получаем название свойства без префикса get
		$_name = '_'.substr(strtolower($name), 3);

		if (strncasecmp($name,'get',3) == 0 AND property_exists($this, $_name))
		{
			return $this->$_name;
		}
		return parent::__call($name,$parameters);
	}
	
	/**
	 * Отправить письмо
	 *
	 * @return bool
	 */
	public function send()
	{
		Yii::import('ext.msSendMail.MailDrivers');
		
		//отправка письма
		return MailDrivers::factory($this->_drivers)->send($this);
	}

	/**
	 * Просмотр письма без отправки
	 *
	 */
	public function debug()
	{
		Yii::import('ext.msSendMail.MailDrivers');

		$mailDriver = MailDrivers::factory($this->_drivers)->setClassMail($this);

		$options = array(
			'header'  => $mailDriver->getHeader(),
			'to'      => $this->_to,
			'from'    => $this->_from,
			'message' => $this->_message,
			'subject' => $this->_subject
		);

		Yii::app()->getWidgetFactory()
			->createWidget($this, 'WidgetMail', array('options' => $options))
			->run();

		Yii::app()->end();
	}

	/**
	 * Задать отправителя письма
	 *
	 * @param string $email
	 * @return Mail
	 */
	public function setFrom($email)
	{
		$this->_from = trim((string)$email);
		
		return $this;
	}
	
	/**
	 * Задать получателя(ей) письма
	 *
	 * @param mixed $email
	 * @return Mail
	 */
	public function setTo($email)
	{
		$this->_to = $this->_processAddresses($email);
		
		return $this;
	}
	
	/**
	 * Задать текст письма
	 *
	 * @param string $message
	 * @return Mail
	 */
	public function setMessage($message)
	{
		$this->_message = trim((string)$message);
		
		return $this;
	}
	
	/**
	 * Задать заголовок письма
	 *
	 * @param string $subject
	 * @return Mail
	 */
	public function setSubject($subject)
	{
		$this->_subject = trim((string)$subject);
		
		return $this;
	}
	
	/**
	 * Преобразование E-Mail адресов
	 *
	 * @param mixed $addresses
	 * @return string
	 */
	protected function _processAddresses($addresses)
	{
		return (is_array($addresses)) ? implode(", ", $addresses) : trim((string)$addresses);
	}
}

/**
 * Класс виджета для просмотра писма
 *
 */
class WidgetMail extends CWidget
{
	public $options;

	/**
	 * Запуск виджета
	 *
	 * @return string
	 */
	public function run()
	{
		$this->render('debug', $this->options);
	}
}