<div class="emailDebug">
	<h2>.: Dumping email</h2>
	<p>The email extension is in debug mode, which means that the email was not actually sent but is dumped below instead</p>
	<h3>Email</h3>
	<strong>To:</strong>
	<?= CHtml::encode($to) ?>
	<br /><strong>Subject:</strong>
	<?= CHtml::encode($subject) ?>
	<div class="emailMessage"><?= $message ?></div>
	<h3>Additional headers</h3>
	<p>
		<?php
			foreach ($header as $value)
			{
				echo CHtml::encode($value)."<br />";
			}
		?>
	</p>
</div>
